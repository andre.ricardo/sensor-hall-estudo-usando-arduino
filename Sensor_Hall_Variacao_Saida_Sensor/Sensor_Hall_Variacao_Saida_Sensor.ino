// Programa : Sensor Hall
// Autor : André Ricardo

#define DEGRAU 4.88
#define TAMANHO 50
#define TEMPO1_LED1 750
#define TEMPO2_LED1 250
#define TEMPO1_LED2 250
#define TEMPO2_LED2 750
 
int pino_hall = 4;   //Pino de sinal do sensor
int nivel;         //Armazena informações sobre a leitura digital do sensor
int analogPin = A0;   //Pino de entrada analógica 
int valor = 0;         // guarda valor analógico da saída do sensor
int pinCampoPresenteBaixo = 7;
int pinCampoPresenteAlto = 8;
int pinCampoAusente = 12;
int medio;             // valor central das leiruras
int leituras[TAMANHO];

void zerarLeituras(){
  for (int i=0; i<TAMANHO; i++){
    leituras[i]=0;
  }
}

int central(int leitura){
  int soma = 0;
  int qtde = 0;
  boolean achou = false;
  for (int i=0; i<TAMANHO; i++){
    if (leituras[i]==leitura){
      achou = true;
      break;
    }
    if (leituras[i]==0){
      if (!(achou)){
        leituras[i]=leitura;
      }
      break;
    }
  }
  Serial.println("leituras(degrau|mV):");
  for (int j=0; j<TAMANHO; j++){
    if (leituras[j]>0){
      qtde += 1;
      soma += leituras[j];
      Serial.print(leituras[j]); 
      Serial.print(" | ");
      Serial.println(leituras[j] * DEGRAU);
    }
  }
  Serial.println("--- end leituras ---");
  Serial.println("quantidade amostras válidas:");
  Serial.println(qtde);
  Serial.println("--- end quantidade ---");
  return soma/qtde;
}

int medioBaixo(int meio){
  int somaMeioBaixo = 0;
  int qtdeBaixo = 0;
  for (int i=0; i<TAMANHO; i++){
    if (leituras[i] != 0 && leituras[i] < meio){
      somaMeioBaixo += leituras[i];
      qtdeBaixo += 1;
    }  
  }
  return somaMeioBaixo/qtdeBaixo;
}

int medioAlto(int meio){
  int somaMeioAlto = 0;
  int qtdeAlto = 0;
  for (int i=0; i<TAMANHO; i++){
    if (leituras[i] != 0 && leituras[i] > meio){
      somaMeioAlto += leituras[i];
      qtdeAlto += 1;
    }  
  }
  return somaMeioAlto/qtdeAlto;
}

void setup(){
  pinMode(pino_hall, INPUT);   
  pinMode(pinCampoPresenteBaixo, OUTPUT);
  pinMode(pinCampoPresenteAlto, OUTPUT);
  pinMode(pinCampoAusente, OUTPUT);
  Serial.begin(9600);
  zerarLeituras();
}
 
void loop(){
  valor = analogRead(analogPin); 
  nivel = digitalRead(pino_hall);
  if (nivel == 0){
    medio = central(valor);
  }
  Serial.println("sinal sensor(0=presente/1=ausente):");
  Serial.println(nivel);
  Serial.println("--- end sensor ---");
  if (nivel ==0){
    Serial.println("Valor analógico sensor(mV):");
    Serial.println(valor * DEGRAU);
    Serial.println("--- end amostra sensor ---");
    Serial.println("valor médio(mV):");
    Serial.println(medio * DEGRAU);
    Serial.println("--- end médio ---"); 
  }
  if (nivel == 0){
    digitalWrite(pinCampoAusente, LOW);
    if (valor > medioAlto(medio)){
      Serial.println("[Verde]tempo(ms):");
      Serial.println(TEMPO1_LED1);
      Serial.println("--- end tempo ---");
      digitalWrite(pinCampoPresenteAlto, HIGH);
      delay(TEMPO1_LED1);
      digitalWrite(pinCampoPresenteAlto, LOW);
      delay(TEMPO1_LED1); 
    }
    if (valor >= medio && valor <= medioAlto(medio)){
      Serial.println("[Verde]tempo(ms):");
      Serial.println(TEMPO2_LED1);
      Serial.println("--- end tempo ---");
      digitalWrite(pinCampoPresenteAlto, HIGH);
      delay(TEMPO2_LED1);
      digitalWrite(pinCampoPresenteAlto, LOW);
      delay(TEMPO2_LED1); 
    }
    if (valor < medio && valor >= medioBaixo(medio)){
      Serial.println("[Vermelho]tempo(ms):");
      Serial.println(TEMPO1_LED2);
      Serial.println("--- end tempo ---");
      digitalWrite(pinCampoPresenteBaixo, HIGH);
      delay(TEMPO1_LED2);
      digitalWrite(pinCampoPresenteBaixo, LOW);
      delay(TEMPO1_LED2);
    }
    if (valor < medioBaixo(medio)){
      Serial.println("[Vermelho]tempo(ms):");
      Serial.println(TEMPO2_LED2);
      Serial.println("--- end tempo ---");
      digitalWrite(pinCampoPresenteBaixo, HIGH);
      delay(TEMPO2_LED2);
      digitalWrite(pinCampoPresenteBaixo, LOW);
      delay(TEMPO2_LED2);
    }
  }
  else{
    digitalWrite(pinCampoAusente, HIGH);
    digitalWrite(pinCampoPresenteBaixo, LOW);
    digitalWrite(pinCampoPresenteAlto, LOW);
  }
}
