# -*- coding: utf-8 -*-

import thread
import serial
import logging
import datetime

class Monitor:

    def __init__(self, run_state=False):
       self.RUN_STATE = run_state

    """" Thread to listen Arduino port """
    def listen_arduino(self, port):
        ser = serial.Serial(port, 9600)
        while self.RUN_STATE:
            line = ser.readline()
            if not line.strip():
                continue
            info = line.replace('\n','')
            logging.debug(info)
            print info
        ser.close()

if __name__ == "__main__":
    now=(datetime.datetime.now().isoformat()).replace(':','.')
    logging.basicConfig(filename='log_'+now+'.log',level=logging.DEBUG, format='[%(asctime)s]: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    m= Monitor()
    m.RUN_STATE = True
    try:
       """ Set the appropriate port """
       thread.start_new_thread(m.listen_arduino, ("COM6",)) 
    except:
       logging.debug("Error: unable to start thread") 

        
